Install Prometheus + kube-state-metric on K8s in kube-system namespace


01. ClusterRoleBinding
02. ClusterRole
03. RoleBinding
04. Role
05. ServiceAccount (Must be created before being referenced) - (prometheus-k8s - kube-state-metrics)**
06. Service_kube-state-metric
07. Service_alermanager
08. Service_exporter
09. Service_prometheus_webui
10. ConfigMap_alertmanager_template
11. ConfigMap_alertmanager_config
12. ConfigMap_prometheus_config
13. ConfigMap_prometheus_rule
14. DaemonSet_exporter
15. DaemonSet_node_directory_size_metrics
16. Deployment Alert Manager v0.15.3 (latest)

17. Deployment Node Exporter v0.17.0 (latest) - donothave
18. Deployment prometheus v2.4.3
19. Deployment kube-state-metric  v1.4.0 (latest)





===============================================================================

Prerequisites:

 - kubelet uses token authentication and authorization:
		--authentication-token-webhook=true	=> ServiceAccount token
		--authorization-mode=Webhook		=> KUBELET will perform an RBAC request with the API to determine, 
							whether the requesting entity (Prometheus in this case) is allow to access a resource, 
							in specific for this project the /metrics endpoint
		
 - Prometheus needs a client certificate => give it full access to the kubelet


================================================================================

https://github.com/coreos/prometheus-operator/tree/master/contrib/kube-prometheus/manifests
https://github.com/giantswarm/kubernetes-prometheus (all*)
https://github.com/kubernetes/kube-state-metrics
https://github.com/prometheus/prometheus
https://github.com/prometheus/node_exporter
https://github.com/prometheus/alertmanager

https://raw.githubusercontent.com/kayrus/prometheus-kubernetes/master/node-exporter-ds.yaml




=================================================================================

Delete full log:

namespace "monitoring" deleted
clusterrolebinding "prometheus" deleted
clusterrole "prometheus" deleted
serviceaccount "prometheus-k8s" deleted
configmap "alertmanager-templates" deleted
configmap "alertmanager" deleted
deployment "alertmanager" deleted
service "alertmanager" deleted
configmap "grafana-import-dashboards" deleted
job "grafana-import-dashboards" deleted
service "grafana" deleted
configmap "prometheus-core" deleted
Error from server (NotFound): error when stopping "manifests-all.yaml": deployments.extensions "grafana-core" not found
Error from server (NotFound): error when deleting "manifests-all.yaml": secrets "grafana" not found
Error from server (NotFound): error when stopping "manifests-all.yaml": deployments.extensions "prometheus-core" not found
Error from server (NotFound): error when stopping "manifests-all.yaml": deployments.extensions "kube-state-metrics" not found
Error from server (NotFound): error when deleting "manifests-all.yaml": serviceaccounts "kube-state-metrics" not found
Error from server (NotFound): error when deleting "manifests-all.yaml": services "kube-state-metrics" not found
Error from server (NotFound): error when stopping "manifests-all.yaml": daemonsets.extensions "node-directory-size-metrics" not found
Error from server (NotFound): error when stopping "manifests-all.yaml": daemonsets.extensions "prometheus-node-exporter" not found
Error from server (NotFound): error when deleting "manifests-all.yaml": services "prometheus-node-exporter" not found
Error from server (NotFound): error when deleting "manifests-all.yaml": configmaps "prometheus-rules" not found
Error from server (NotFound): error when deleting "manifests-all.yaml": services "prometheus" not found
